/*
    Copyright 2020 Marek Kuethe
    This program is free software. It comes without any warranty, to
    the extent permitted by applicable law. You can redistribute it
    and/or modify it under the terms of the Do What The Fuck You Want
    To Public License, Version 2, as published by Sam Hocevar. See
    http://www.wtfpl.net/ for more details.
*/
#include "mboblib.hpp"

#define BERROR(extra) if(berr extra) throw berr

// mBOBResult

mBOBResult::mBOBResult(std::string res)
{
    size_t beg;
    for(beg = 0; res[beg] == '\n' || res[beg] == ' '; beg++);
    size_t i;
    for(i = beg; res[i] != ' '; i++);
    this->status = res.substr(beg, i - beg);
    
    if(this->status != "DATA")
    {
        size_t j;
        for(j = i + 1; res[j] != '\n' && j < res.length(); j++);
        if(j == i + 1)
            this->content = "";
        else
            this->content = res.substr(i + 1, j - ( i + 1 ));
        this->data = "";
    }
    else
    {
        size_t j;
        this->data = "DATA ";
        for(j = i + 1;;j++)
        {
            if(res[j] == 'O' && res[j+1] == 'K')
                break;
            this->data.push_back(res[j]);
        }
        this->status = res.substr(j, 2);
        
        size_t k;
        for(k = j + 3; res[k] != '\n' && k < res.length(); k++);
        this->content = res.substr(j + 3, k - ( j + 3 ));
    }
}

mBOBResult::mBOBResult(std::string status, std::string content)
    : status(status), content(content)
{
    
}

bool mBOBResult::issuccessful()
{
    return ( this->status=="OK"?true:false );
}

std::string mBOBResult::getstatus()
{
    return this->status;
}

std::string mBOBResult::getcontent()
{
    return this->content;
}

std::string mBOBResult::getdata()
{
    return this->data;
}

// mBOBClient

mBOBClient::mBOBClient(std::string host, unsigned short port)
    : host(host), port(port)
{
    this->ios = std::make_shared<boost::asio::io_service>();
    this->sock = std::make_shared<boost::asio::ip::tcp::socket>(* this->ios);
    
    boost::system::error_code berr;
    
    boost::asio::ip::tcp::endpoint ep(boost::asio::ip::address::from_string(host.c_str()), port);
    this->sock->connect(ep, berr);
    BERROR( );
    
    this->bobversion = readsocket(15).substr(4, 8);
}

mBOBClient::~mBOBClient()
{
    this->close();
}

std::string mBOBClient::readsocket(size_t min)
{
    boost::system::error_code berr;
    boost::asio::streambuf buf;
    boost::asio::read(* this->sock, buf, boost::asio::transfer_at_least(14), berr);
    BERROR(&& berr != boost::asio::error::eof);
    
    return boost::asio::buffer_cast<const char *>( buf.data() );
}

void mBOBClient::writesocket(std::string msg)
{
    boost::system::error_code berr;
    boost::asio::write(* this->sock, boost::asio::buffer(msg), berr);
    BERROR( );
}

std::string mBOBClient::getbobversion()
{
    return this->bobversion;
}

mBOBResult mBOBClient::bhelp(std::string p)
{
    if(p == "")
        writesocket("help\n");
    else
        writesocket("help " + p + "\n");
    
    return mBOBResult(readsocket(5));
}

std::string mBOBClient::direct(std::string cmd, size_t min)
{
    writesocket(cmd);
    return readsocket(min);
}

mBOBResult mBOBClient::bclear()
{
    writesocket("clear\n");
    return mBOBResult(readsocket(5));
}

mBOBResult mBOBClient::bgetdest()
{
    writesocket("getdest\n");
    return mBOBResult(readsocket(5));
}

mBOBResult mBOBClient::bgetkeys()
{
    writesocket("getkeys\n");
    return mBOBResult(readsocket(5));
}

mBOBResult mBOBClient::bgetnick()
{
    writesocket("getnick\n");
    return mBOBResult(readsocket(5));
}

mBOBResult mBOBClient::binhost(std::string msg)
{
    writesocket("inhost " + msg + "\n");
    return mBOBResult(readsocket(5));
}

mBOBResult mBOBClient::binport(std::string msg)
{
    writesocket("inport " + msg + "\n");
    return mBOBResult(readsocket(5));
}

mBOBResult mBOBClient::blist()
{
    writesocket("list\n");
    return mBOBResult(readsocket(5));
}

mBOBResult mBOBClient::blookup(std::string msg)
{
    writesocket("lookup " + msg + "\n");
    return mBOBResult(readsocket(5));
}

mBOBResult mBOBClient::bnewkeys()
{
    writesocket("newkeys\n");
    return mBOBResult(readsocket(5));
}

mBOBResult mBOBClient::boption(std::string msg)
{
    writesocket("option " + msg + "\n");
    return mBOBResult(readsocket(5));
}

mBOBResult mBOBClient::bouthost(std::string msg)
{
    writesocket("outhost " + msg + "\n");
    return mBOBResult(readsocket(5));
}

mBOBResult mBOBClient::boutport(std::string msg)
{
    writesocket("boutport " + msg + "\n");
    return mBOBResult(readsocket(5));
}

mBOBResult mBOBClient::bquiet(bool msg)
{
    writesocket("boutport " + (msg==true?std::string("True"):std::string("False")) + "\n");
    return mBOBResult(readsocket(5));
}

mBOBResult mBOBClient::bsetkeys(std::string msg)
{
    writesocket("setkeys " + msg + "\n");
    return mBOBResult(readsocket(5));
}

mBOBResult mBOBClient::bsetnick(std::string msg)
{
    writesocket("setnick " + msg + "\n");
    return mBOBResult(readsocket(5));
}

mBOBResult mBOBClient::bshow()
{
    writesocket("show\n");
    return mBOBResult(readsocket(5));
}

mBOBResult mBOBClient::bshowprops()
{
    writesocket("showprops\n");
    return mBOBResult(readsocket(5));
}

mBOBResult mBOBClient::bstart()
{
    writesocket("start\n");
    return mBOBResult(readsocket(5));
}

mBOBResult mBOBClient::bstatus(std::string msg)
{
    writesocket("status " + msg + "\n");
    return mBOBResult(readsocket(5));
}

mBOBResult mBOBClient::bstop()
{
    writesocket("stop\n");
    return mBOBResult(readsocket(5));
}

mBOBResult mBOBClient::bverify(std::string msg)
{
    writesocket("verify " + msg + "\n");
    return mBOBResult(readsocket(5));
}

mBOBResult mBOBClient::bvisit()
{
    writesocket("visit\n");
    return mBOBResult(readsocket(5));
}

mBOBResult mBOBClient::bzap()
{
    writesocket("zap\n");
    return mBOBResult(readsocket(5));
}

void mBOBClient::readmore(mBOBResult * mbr)
{
    mbr->content += readsocket(1);
}

bool mBOBClient::close()
{
    writesocket("quit\n");
    mBOBResult res = readsocket(5);
    (* this->sock).close();
    return res.issuccessful();
}