=begin
  Copyright (c) 2020 Marek Küthe
  This program is free software. It comes without any warranty, to
  the extent permitted by applicable law. You can redistribute it
  and/or modify it under the terms of the Do What The Fuck You Want
  To Public License, Version 2, as published by Sam Hocevar. See
  http://www.wtfpl.net/ for more details.
=end

class I2PBlocklistParser
  
  attr_reader :data, :log, :ipv4s, :ipv4_datetimes, :ipv6s, :ipv6_datetimes, :p, :ep, :unknowns
  
  def self.initializeFromFile filename = "blocklist.txt"
    return self.new File.read "blocklist.txt"
  end
  
  def initialize dat
    @data = dat
    @ep = false
    @p = false
  end
  
  def parse!
    require "date"
    #DateTime.parse("Wed Oct 30 15:44:29 GMT 2019").strftime("%d/%m/%Y %H:%M:%S %z")
    lins = @data.lines
    cnt = 0
    @log = String.new
    @unknowns = Array.new
    @ipv4s = Array.new
    @ipv4s_datetimes = Array.new
    @ipv6s = Array.new
    @ipv6s_datetimes = Array.new
    
    while cnt < lins.length
      if lins[cnt].start_with? "#"
        @log << "comment: #{lins[cnt][1..-1].chomp}\n"
      elsif lins[cnt] =~ /Blocklist feed (.* .* [1-3]?[0-9] [1-2]?[0-9]:[1-5]?[0-9]:[1-5]?[0-9] GMT [0-9]+):(.*)/
        tim = DateTime.parse($1).strftime("%d/%m/%Y %H:%M:%S %z")
        ip = $2
        if ip.include? ";"
          ip.gsub! ";", ":"
          ip = "[#{ip}]"
          @log << "#{tim} {#{ip}} [ipv6]\n"
          @ipv6s << ip
          @ipv6s_datetimes << tim
        else
          @log << "#{tim} {#{ip}} [ipv4]\n"
          @ipv4s << ip
          @ipv4s_datetimes << tim
        end
      elsif
        @log << "error: {#{lins[cnt]}}"
        @unknowns << lins[cnt]
      end
      
      cnt += 1
    end
    
    @p = true
    return self
  end
  
  def extraparse!
    if not @p
      return false
    end
    @ep = true
    @aips = @ipv4s + @ipv6s
    @adatetimes = @ipv4s_datetimes + @ipv6s_datetimes
    return self
  end
  
  def allips
    if not @p
      return false
    elsif @ep
      return @aips
    else
      return @ipv4s + @ipv6s
    end
  end
  
  def alldatetimes
    if not @p
      return false
    elsif @ep
      return @adatetimes
    else
      return @ipv4s_datetimes + @ipv6s_datetimes
    end
  end
end

require "pp"

parser = I2PBlocklistParser.initializeFromFile
parser.parse!

pp parser.allips
pp parser.unknowns
