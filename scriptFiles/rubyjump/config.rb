
# example for fetch a hosts.txt to a hosts.gdbm
#  ruby fetch.rb "http://identiguy.i2p/hosts.txt" 127.0.0.1 4444
# to run the jump service run
#  ruby jumper.rb

$host               = "127.0.0.1"
$port               = 8080
$gdbm_file          = "hosts.gdbm"
$provide_hosts_txt  = true