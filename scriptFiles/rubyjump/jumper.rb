puts "Load required modules"

require "socket"
require "erb"
require "gdbm"

require_relative "mhttp.rb"
require_relative "config.rb"

puts "Read database"
$db = GDBM.new $gdbm_file
END { $db.close }

puts "Open tcp server"
serv = TCPServer.new $host, $port

if $provide_hosts_txt
  puts "Create hosts.txt"
  $hoststxt = String.new
  $db.each_pair { |eepsite, base64|
    $hoststxt += "#{eepsite}=#{base64}\n"
  }
end

puts "Start server"
loop do
  Thread.start(serv.accept) { |sock|
    resp = MHTTP::ServResponse.fromSocket(sock).parse!
    path = resp.meta[:path]
    if path == "/" # home
      sock.puts MHTTP::ServRequest.fromResponse(resp, "200", "OK", {
        "Date" => Time.new,
        "Server" => "rubyjump",
        "Connection" => "close",
        "Content-Type" => "text/html; charset=UTF-8"
      }, ERB.new(File.read("./templates/home.erb")).result(binding)).prepare!.request
    elsif path[0...6] == "/jump/" # jumper
      eepsite = path[6..-1].delete_suffix "/"
      b64 = $db[eepsite]
      
      p eepsite
      if b64 and eepsite != ""
        title = "Jump to #{eepsite}"
        redirect = "http://#{eepsite}/?i2paddresshelper=#{b64}"
        sock.puts MHTTP::ServRequest.fromResponse(resp, "200", "OK", {
          "Date" => Time.new,
          "Server" => "rubyjump",
          "Connection" => "close",
          "Content-Type" => "text/html; charset=UTF-8"
        }, ERB.new(File.read("./templates/jump.erb")).result(binding)).prepare!.request
      else
        title = "Jump to #{eepsite}"
        sock.puts MHTTP::ServRequest.fromResponse(resp, "200", "OK", {
          "Date" => Time.new,
          "Server" => "rubyjump",
          "Connection" => "close",
          "Content-Type" => "text/html; charset=UTF-8"
        }, ERB.new(File.read("./templates/nfjump.erb")).result(binding)).prepare!.request
      end
    elsif path == "/hosts.txt" and $provide_hosts_txt
      sock.puts MHTTP::ServRequest.fromResponse(resp, "200", "OK", {
        "Date" => Time.new,
        "Server" => "rubyjump",
        "Connection" => "close",
        "Content-Type" => "text/html; charset=UTF-8"
      }, $hoststxt).prepare!.request
    else # 404 not found
      sock.puts MHTTP::ServRequest.fromResponse(resp, "404", "Not Found", {
        "Date" => Time.new,
        "Server" => "rubyjump",
        "Connection" => "close",
        "Content-Type" => "text/html; charset=UTF-8"
      }, ERB.new(File.read("./templates/notfound.erb")).result(binding)).prepare!.request
    end
    sock.close
  }
end