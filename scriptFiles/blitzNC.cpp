#include <BOB3.h>
#include <stddef.h>

void blitzNC(size_t, int, int, int, int);

void setup()
{
	blitzNC(10, EYE_1, EYE_2, ROYALBLUE, 40);
}

void loop()
{
	
}

void blitzNC(size_t n, int l1, int l2, int col, int time)
{
    for(size_t i = 0; i < n; i++)
    {
        for(int j = l1; j <= l2; j++)
            bob3.setLed(j, col);
        
        delay(time);
    
        for(int j = l1; j <= l2; j++)
            bob3.setLed(j, OFF);
    
        delay(time);
    }
}