// Copyright 2019 Marek Kuethe

/*

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef PREPARE_STRING
#define PREPARE_STRING

#include <stddef.h>

/*
    The first argument gives a pointer to the string to be prepared.
    The second argument specifies the length of the string to be prepared.
    The third argument decides how to handle unknown characters. If the third
    argument is 1, unknown characters are replaced by an underscore (_),
    otherwise nothing is substituted. The function returns a pointer to the
    prepared string. This is requested with malloc, so it should be released
    with free again. If an error occurs during processing, the function
    returns a NULL pointer.
*/
char * prepare_string(char *, size_t, short);

#endif