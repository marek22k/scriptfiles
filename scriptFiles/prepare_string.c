#include "prepare_string.h"
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

char * prepare_string(char * str, size_t len, short rep)
{
    char * res = malloc(sizeof(char) * len * 2);
    
    if(res == NULL)
        return NULL;
    
    size_t ptr = 0;
    
    for(size_t s = 0; s < len && str[s] != '\0'; s++)
    {
        /*
           The allowed characters are thus included in the result string,
           the verbal characters are noted with a Bach slash before the character.
           Characters which can not be found on any list are replaced by an
           underscore (_) in rep = 1, otherwise by nothing.
         */
        if(isalnum(str[s]) ||
                isspace(str[s]) ||
                /* allowed characters */
                str[s] == '.' ||
                str[s] == ',' ||
                str[s] == '-' ||
                str[s] == '_' ||
                str[s] == '#' ||
                str[s] == '+' ||
                str[s] == '*' ||
                str[s] == '*' ||
                str[s] == '(' ||
                str[s] == ')' ||
                str[s] == '[' ||
                str[s] == ']' ||
                str[s] == '{' ||
                str[s] == '}' ||
                str[s] == '/' ||
                str[s] == '%' ||
                str[s] == '!')
            res[ptr] = str[s];
            /* forbidden characters */
        else if(str[s] == '<' ||
                str[s] == '>' ||
                str[s] == '&' ||
                str[s] == '|')
        {
            res[ptr] = '\\';
            ptr++;
            res[ptr] = str[s];
        }
        else if(rep == 1)
            res[ptr] = '_';
        else
            ptr--;
            
        ptr++;
    }
    res[ptr] = '\0';
    
    return res;
}