/* Copyright 2019 Marek Kuethe */
/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <regex>

#include <cstdlib>
#include <cstddef>

#include "mfcplib.hpp"

/* mFCPClient, PriorityClass, Identifier/ClientToken, URI */
std::string fcpGetRowContent(mFCP::mFCPClient &, short, std::string, std::string);

int main()
{
    /* Example usage */
    mFCP::mFCPClient client("MyTestClient", "94.16.122.252");
    
    std::string str = fcpGetRowContent(client, 1, "Request", "USK@940RYvj1-aowEHGsb5HeMTigq8gnV14pbKNsIvUO~-0,FdTbR3gIz21QNfDtnK~MiWgAf2kfwHe-cpyJXuLHdOE,AQACAAE/publish/3/");
    std::cout << str << std::endl;
    
    return EXIT_SUCCESS;
}

std::string fcpGetRowContent(mFCP::mFCPClient & client, short priority, std::string identifier, std::string uri)
{
    /* Send request */
    
    client.sendmsg("ClientGet\n"
                                "URI=" + uri + "\n"
                                "ReturnType=direct\n"
                                "Verbosity=1\n"
                                "Identifier=" + identifier + "\n"
                                "ClientToken=" + identifier + "\n"
                                "Global=false"
                                "PriorityClass=" + std::to_string(priority) + "\n"
                                "Persistence=connection\n"
                                "EndMessage\n");
    
    /* Wait for the request to complete successfully */
    
    std::string str;
    std::smatch m;
    bool res;
    
    {
        std::regex reg("DataLength=(.*)\n");
        str = client.wait(1);
        res = std::regex_search(str, m, reg);
    
        //while(! (res && m.size() > 1))
        while( (! res) || m.size() <= 1 )
        {
            str = client.wait(1);
            res = std::regex_search(str, m, reg);
        }
    }
    
    {
        /* Determine the length of the entire result */
        size_t len = static_cast<size_t>(std::stoul(m.str(1)));
    
        /* Cut out the already partially submitted request */
        std::regex reg("\nData\n(.*)", std::regex_constants::extended);
        //std::smatch m;
        res = std::regex_search(str, m, reg);
        if( (! res) || m.size() <= 1 ) throw std::runtime_error(std::to_string(__LINE__));
    
        /* Query remaining query */
        str = m.str(1) + client.wait(len - m.str(1).length());
    }
    
    return str;
}
