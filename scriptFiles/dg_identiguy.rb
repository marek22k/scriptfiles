
require "json"
require "rmagick"
require "gruff"
require "date"

counts = Array.new
dates = Array.new
i = 0

Dir["dat_*.json"].each { |fn|
  counts << JSON.load(File.read fn)[0]
  
  fn[4 + 13] = ":"
  fn[4 + 16] = ":"
  dates << DateTime.parse(fn).strftime("%m/%d")
  
  if i == 7
    drawGraph counts, dates
    counts.clear
    dates.clear
    i = 0
  end
}

def drawGraph counts, dates
  rd = Hash.new
  i = 0
  dates.each { |d|
    rd[i] = d
    i += 1
  }
  graph = Gruff::Line.new(1000)
  graph.title = "Eepsites"
  graph.hide_lines = false
  graph.labels = rd
  graph.theme = {
    :colors => ['#3B5998'],
    :marker_color => 'silver',
    :font_color => 'black',
    :background_colors => ['white', 'silver']
  }
  graph.data("Number of active eepsites", counts.collect)
  
  graph.write "#{dates[0].strftime("%Y-%m-%d")}_#{dates[-1].strftime("%Y-%m-%d")}.png"
end

