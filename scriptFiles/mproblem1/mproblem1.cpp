// Copyright 2019 Marek Kuethe

/*

   This file is part of mproblem1.

    mproblem1 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    mproblem1 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with mproblem1.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <iostream>
#include <stdexcept>
#include <exception>

#include <lua.hpp>

#include <cstdlib>

int main()
{
    try
    {
        lua_State * l = luaL_newstate();
        luaL_openlibs(l);
    
        if(luaL_dofile(l, "config.lua"))
            throw std::runtime_error("Can not parse config.lua");
    
        lua_getglobal(l, "vbegin");
        int b = luaL_checkinteger(l, -1);
    
        lua_getglobal(l, "vend");
        int e = luaL_checkinteger(l, -1);
    
        lua_settop(l, 0);
        
        int sum = 0;
        for(int i = b; i < e; i++)
        {
            lua_getglobal(l, "check");
            lua_pushinteger(l, i);
            if(lua_pcall(l, 1, 1, 0))
                throw std::runtime_error("Can not call function check");
            
            if(! lua_isboolean(l, -1))
                throw std::runtime_error("Lua function check does not return a boolean");
            
            if(lua_toboolean(l, -1))
            {
                std::cout << i << ", ";
                sum += i;
            }
        }
    
        std::cout << "\b\b = " << sum << std::endl;
    
        lua_close(l);
    }
    catch(const std::exception & e)
    {
        std::cerr << "Error: " << e.what() << std::endl;
        exit(EXIT_FAILURE);
    }
    
    return EXIT_SUCCESS;
}