/*
    This is a modified version of the code from the link below.
    
    "The standard was published in 1998, followed by a revision in 2003. Some compilers older than the standard implement already some of its features, and many newer compilers don't implement all ANSI-C++ features. If you have doubts on whether your compiler will be able to compile ANSI-C++ code, you can try to compile a piece of code with some of the new features introduced mainly after the publication of the standard. For example, the following code fragment uses the bool type, and uses namespaces and templates."
    
    Source: http://www.cplusplus.com/info/faq/ - How may I know if my compiler supports ANSI-C++?
*/

#include <iostream>

using namespace std;

template <class T>
bool ansisupported(T x)
{
    return true;
}

int main()
{
    if (ansisupported(0))
        cout << "ANSI OK: Your compiler supports ANSI C++." << endl;
    
    return 0;
}

