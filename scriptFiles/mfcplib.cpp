/* Copyright 2019 Marek Kuethe */
/*
    This file is part of mfcplib.

    mfcplib is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    mfcplib is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with mfcplib.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mfcplib.hpp"

using namespace mFCP;

mFCPClient::mFCPClient(std::string name, std::string hostname, unsigned short port)
                : hostname(hostname), port(port), name(name)
{
    this->ios = std::make_shared<boost::asio::io_service>();
    this->sock = std::make_shared<boost::asio::ip::tcp::socket>(* this->ios);
    
    boost::system::error_code berr;
    
    boost::asio::ip::tcp::endpoint ep(boost::asio::ip::address::from_string(this->hostname.c_str()), port);
    this->sock->connect(ep, berr);
    if(berr) throw berr;
    
    std::string msg = std::string("ClientHello\n"
                                    "Name=" + this->name + "\n"
                                    "ExpectedVersion=2.0\n"
                                    "EndMessage\n");
    this->NodeHello = this->sendmsg(msg);
    
}

std::string mFCPClient::sendmsg(std::string msg, size_t min)
{
    boost::system::error_code berr;
    
    boost::asio::write(* this->sock, boost::asio::buffer(msg), berr);
    if(berr) throw berr;
    
    boost::asio::streambuf buf;
    boost::asio::read(* this->sock, buf, boost::asio::transfer_at_least(min), berr);
    if(berr && berr != boost::asio::error::eof) throw berr;
    
    return boost::asio::buffer_cast<const char *>( buf.data() );
}

std::string mFCPClient::getNodeHello()
{
    return this->NodeHello;
}

std::string mFCPClient::fcpListPeer(std::string NodeIdentifier,
                            bool WithVolatile, bool WithMetadata)
{
    std::string msg = std::string("ListPeer\n"
                                    "NodeIdentifier=" + NodeIdentifier + "\n"
                                    "WithVolatile=" + (WithVolatile==true?"true":"false") + "\n"
                                    "WithMetadata=" + (WithMetadata==true?"true":"false") + "\n"
                                    "EndMessage\n");
    return this->sendmsg(msg);
}

std::string mFCPClient::fcpListPeers(std::string Identifier,
                            bool WithVolatile, bool WithMetadata)
{
    std::string msg = std::string(std::string("ListPeers\n")
                                    + (Identifier==""?"": ("Identifier=" + Identifier + "\n") ) +
                                    "WithVolatile=" + (WithVolatile==true?"true":"false") + "\n"
                                    "WithMetadata=" + (WithMetadata==true?"true":"false") + "\n"
                                    "EndMessage\n");
    return this->sendmsg(msg);
}

std::string mFCPClient::fcpListPeerNotes(std::string NodeIdentifier)
{
    std::string msg = std::string("ListPeerNotes\n"
                                    "NodeIdentifier=" + NodeIdentifier + "\n"
                                    "EndMessage\n");
    return this->sendmsg(msg);
}

std::string mFCPClient::fcpAddPeer(mFCPTrust Trust, mFCPVisibility Visibility, std::string ref)
{
    std::string msg = std::string(std::string("AddPeer\n") +
                                    "Trust=" + (Trust==LOW?"LOW": Trust==NORMAL?"NORMAL":"HIGH" ) + "\n"
                                    "Visibility=" + (Visibility==NO?"NO": Visibility==NAME_ONLY?"NAME_ONLY":"YES" ) + "\n"
                                    + ref + "\n"
                                    "EndMessage\n");
    return this->sendmsg(msg);
}

std::string mFCPClient::fcpGetNode(std::string Identifier, bool GiveOpennetRef, bool WithPrivate, bool WithVolatile)
{
    std::string msg = std::string(std::string("GetNode\n")
                                    + (Identifier==""?"":"Identifier=" + Identifier + "\n") +
                                    "GiveOpennetRef=" + (GiveOpennetRef==true?"true":"false") + "\n"
                                    "WithPrivate=" + (WithPrivate==true?"true":"false") + "\n"
                                    "WithVolatile=" + (WithVolatile==true?"true":"false") + "\n"
                                    "EndMessage\n");
    return this->sendmsg(msg);
}

std::string mFCPClient::fcpGenerateSSK(std::string Identifier)
{
    std::string msg = std::string(std::string("GenerateSSK\n")
                                    + (Identifier==""?"":"Identifier=" + Identifier + "\n") +
                                    "EndMessage\n");
    return this->sendmsg(msg);
}

std::string mFCPClient::fcpGetRequestStatus(std::string Identifier, bool Global, bool OnlyData)
{
    std::string msg = std::string(std::string("GetRequestStatus\n") +
                                    "Identifier=" + Identifier + "\n"
                                    "Global=" + (Global==true?"true":"false") + "\n"
                                    "OnlyData=" + (OnlyData==true?"true":"false") + "\n"
                                    "EndMessage\n");
    return this->sendmsg(msg);
}

std::string mFCPClient::fcpRemoveRequest(std::string Identifier, bool Global)
{
    std::string msg = std::string(std::string("RemoveRequest\n") +
                                    "Identifier=" + Identifier + "\n"
                                    "Global=" + (Global==true?"true":"false") + "\n"
                                    "EndMessage\n");
    return this->sendmsg(msg);
}

void mFCPClient::fcpDisconnect()
{
    std::string msg = std::string("Disconnect\n"
                                    "EndMessage\n");
    this->sendmsg(msg);
}

void mFCPClient::fcpShutdown(bool shutdown)
{
    if(shutdown)
    {
        std::string msg = std::string("Shutdown\n"
                                        "EndMessage\n");
        this->sendmsg(msg); 
    }
}

mFCPClient::~mFCPClient()
{
    this->fcpDisconnect();
    (* this->sock).close();
}

/* # 2 # */

std::string mFCPClient::wait(size_t count)
{
    boost::system::error_code berr;
    
    boost::asio::streambuf buf;
    boost::asio::read(* this->sock, buf, boost::asio::transfer_at_least(count), berr);
    if(berr && berr != boost::asio::error::eof) throw berr;
    
    return boost::asio::buffer_cast<const char *>( buf.data() );
}