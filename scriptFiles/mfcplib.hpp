/* Copyright 2019 Marek Kuethe */
/*
    This file is part of mfcplib.

    mfcplib is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    mfcplib is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with mfcplib.  If not, see <http://www.gnu.org/licenses/>.
*/

/*

 * Dieses Programm habe ich in meiner Freizeit geschrieben.
 * Sollten Fehler im Programm sein, wuerde ich mich ueber eine Rueckmeldung freuen
 * Marek Kuethe - m.k@mk16.de

Auf meinem Debian GNU/Linux PC habe ich es mit folgender Makefile kompiliert:
prog: main.o mfcplib.o
	g++ main.o mfcplib.o -std=c++17 -o a.out -lboost_system -pthread
	
main.o: main.cpp mfcplib.hpp
	g++ -c main.cpp -o main.o -Wall -Wextra -Wpedantic -std=c++17 
	
mfcplib.o: mfcplib.cpp mfcplib.hpp
	g++ -c mfcplib.cpp -o mfcplib.o -Wno-reorder -std=c++17 

Beispiel-Programm(main.cpp):

#include <iostream>

#include <cstdlib>

#include "mfcplib.hpp"

using namespace std;
using namespace mFCP;

int main()
{
    mFCPClient cli1("TestClient");
    
    cout << cli1.getNodeHello() << endl;
    //cout << cli1.fcpListPeer("[UB] UberNode", "false", "true") << endl;
    //cout << cli1.fcpListPeers("", true, true) << endl;
    //cout << cli1.fcpListPeerNotes("Hij7qd5KUvHNUKgi5l6u423PqMkMLlWHiTUSjBsaQKA") << endl;
    //cout << cli1.fcpGetNode() << endl;
    //cout << cli1.fcpGenerateSSK("hi") << endl;
    
    return EXIT_SUCCESS;
}
 
*/

#ifndef MFCPLIB_HPP
#define MFCPLIB_HPP

#include <boost/asio.hpp>

#include <string>
#include <stdexcept>
#include <new>

#include <cstddef>

namespace mFCP
{

enum mFCPTrust
{
    LOW, NORMAL, HIGH
};

enum mFCPVisibility
{
    NO, NAME_ONLY, YES
};

enum mFCPPersistence
{
    connection, reboot, forever
};

enum mFCPUploadFrom
{
    direct, disk, redirect
};

class mFCPClient
{
    protected:
        std::shared_ptr<boost::asio::io_service> ios;
        std::shared_ptr<boost::asio::ip::tcp::endpoint> ep;
        std::shared_ptr<boost::asio::ip::tcp::socket> sock;
        std::string name;
        std::string hostname;
        std::string NodeHello;
        unsigned short port;
        
    public:
        mFCPClient(std::string, std::string = "127.0.0.1", unsigned short = 9481);
        /* Die Funktion sendmsg sendet eine rohe(raw) Nachricht (message).
           Diese sollte mit einem Zeilenumbruch sowie "EndMessage" beendet werden.
           Der zweite Parameter ist fuer die Boost Bibliothek. Es gibt an, wie viele
           Zeichen mindestens zuruck gefordert werden sollen. Hier wird die Funktion
           erst beendet. */
        std::string sendmsg(std::string msg, size_t = 1);
        /* Die Funktion gibt die Antwort nach der ClientHello Nachricht zurueck. */
        std::string getNodeHello();
        /* Eine Referenz zu den Knoten mit dem Prefix "fcp" kann man grundsaetzlich auf
           https://github.com/freenet/wiki/wiki/FCPv2 finden. */
        std::string fcpListPeer(std::string, bool = false, bool = false);
        std::string fcpListPeers(std::string = "", bool = false, bool = false);
        std::string fcpListPeerNotes(std::string);
        /* | Der letzte String kann entweder File=, URL= sein oder die rohe(raw) Knoten
           V Referenz enthalten. Weitere Hinweis dazu gibt es auf https://github.com/freenet/wiki/wiki/FCPv2-AddPeer */
        std::string fcpAddPeer(mFCPTrust, mFCPVisibility, std::string);
        /* Die anderen Peer-Bearbeitungs Optionen sind fuer mich etwas zu komplex;
           Diese koennen aber via sendmsg gesendet werden. */
        std::string fcpGetNode(std::string = "", bool = false, bool = false, bool = false);
        std::string fcpGenerateSSK(std::string = "");
        /* Die weiteren Client*-Funktionen vom FCP habe ich aus Gruenden der Komplexitaet und der mangelnden Motivation nicht implementiert. */
        std::string fcpGetRequestStatus(std::string, bool = false, bool = false);
        std::string fcpRemoveRequest(std::string, bool = false);
        /* Trennt die Verbindung zum Freenet-Node; Danach kann der FCPClient nicht mehr benutzt werden;
           Der Destructor ruft diese Funktion auch auf. */
        void fcpDisconnect();
        /* Der Knoten wird nur heruntergefahren, wenn true als Argument uebergeben wird.
           Dies soll zur Sicherheit dienen. (Damit man ihn nicht  versehentlich ausschaltet) */
        void fcpShutdown(bool);
        ~mFCPClient();
        
        /* # 2 # */
        std::string wait(size_t);
};

}

#endif  // MFCPLIB_HPP
