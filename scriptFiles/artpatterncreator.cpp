// Author: Marek K.

/*

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Dieses Programm ist Freie Software: Sie koennen es unter den Bedingungen
    der GNU General Public License, wie von der Free Software Foundation,
    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
    veroeffentlichten Version, weiter verteilen und/oder modifizieren.

    Dieses Programm wird in der Hoffnung bereitgestellt, dass es nuetzlich sein wird, jedoch
    OHNE JEDE GEWAEHR,; sogar ohne die implizite
    Gewaehr der MARKTFAEHIGKEIT oder EIGNUNG FUER EINEN BESTIMMTEN ZWECK.
    Siehe die GNU General Public License fuer weitere Einzelheiten.

    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
    Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.

*/

/*
    German
    ======

Programm zum erstellen von Mustern:

Es gibt zwei Modis:
1. Schreib alles Fortlaufend
Beipsiel:
A
BC
DEF
2. Faengt immer neu an
Beispiel:
A
AB
ABC
Dies wird als erstes Argument angegeben(1 oder 2). Ohne Argument ist der Standard 1.

Das zweite Argument gibt die Anzahl der Zeilen an(Standard: 9).

Fuer das dritte und vierte Argument gibt es zwei Moeglichkeiten:
1. Reihe von der ASCII-Tabelle verwenden.
Beispiel: A Z
Alle Zeichen von A bis Z. Also: ABCDEFGHIJKLMNOPQRSTUVWXYZ
Oder
Beispiel: B G: BCDEFG
2. Selbstdefinierte Reihe
Ein Ohne-Leerzeichen-Zeichenkette oder ein Mit-Leerzeichen-Zeichenkette in Gaenstefueszchen.
Beispiel: Hello
Oder
Beispiel: "Hello World"

Beispiele vom Ausruf des Programnmes:
Beispiel 1:
[PROGRAMM] 1 5 A E
erzeugt:
A
BC
DEA
BCDE
ABCDE
 
Beispiel 2:
[PROGRAMM] 2 5 AE
erzeugt:
A
AB
ABC
ABCD
ABCDE

    English
    =======

Program to create patterns:

There are two modes:
1. Write everything continuously
Beipsiel:
A
BC
DEF
2. Always starts again
Example:
A
FROM
ABC
This is stated as the first argument (1 or 2). Without argument, the default is 1.

The second argument specifies the number of lines (default: 9).

There are two possibilities for the third and fourth arguments:
Use 1st row from the ASCII table.
Example: A Z
All characters from A to Z. Also: ABCDEFGHIJKLMNOPQRSTUVWXYZ
Or
Example: B G: BCDEFG
2. Self-defined series
A no-space string or a whitespace-in-a-row string.
Example: Hello
Or
Example: "Hello World"

Examples of the program's exclamation:
Example 1:
[PROGRAM] 1 5 A E
generated:
A
BC
DEA
BCDE
ABCDE
 
Example 2:
[PROGRAM] 2 5 AE
generated:
A
FROM
ABC
ABCD
ABCDE
*/

#include <iostream>
#include <stdexcept>
#include <exception>
#include <string>

#include <cstdlib>
#include <cerrno>
#include <cstring>

using namespace std;

int main(int argc, char * argv[])
{
    try
    {
        char s = 'A';
        char e = 'Z';
        unsigned n = 9;
        bool set = true;
        string str;
        short mode = 1;
        
        if(argc > 1)
        {
            if(argv[1][0] == '2')
                mode = 2;
        }
        if(argc > 2)
        {
            errno = 0;
            n = strtoul(argv[2], NULL, 0);
            if(errno == ERANGE)
                throw runtime_error("ERANGE");
            
            if(argc > 3 && strlen(argv[3]) > 1)
            {
                str = string(argv[3]);
                set = false;
            }
            else if(argc > 4)
            {
                s = argv[3][0];
                e = argv[4][0];
            }
        }
        
        if(set)
        {
            for(char c = s; c <= e; c++)
            {
                str += c;
            }
        }
        
        unsigned c = 0;
        cout << str << endl << endl;
        
        if(mode == 2)
        {
            cout << str[0] << endl;
            n--;
        }
            
        for(unsigned i = 1; i <= n; i++)
        {
            if(mode == 1)
            {
                for(unsigned j = 1; j <= i; j++)
                {
                    cout << str[c];
                    if(c++ == str.length()-1)
                        c = 0;
                }
            }
            else if(mode == 2)
            {
                cout << str.substr(0, (i%str.length())+1);
            }
            cout << endl;
        }
    }
    catch(const exception & e)
    {
        cerr << "Error: " << e.what() << endl;
        exit(EXIT_FAILURE);
    }
    
    return EXIT_SUCCESS;
}