=begin
  This program uses I2P's BOB API to perform various minor operations.

  Copyright 2020 Marek Küthe
  This program is free software. It comes without any warranty, to
  the extent permitted by applicable law. You can redistribute it
  and/or modify it under the terms of the Do What The Fuck You Want
  To Public License, Version 2, as published by Sam Hocevar. See
  http://www.wtfpl.net/ for more details.
=end

require "socket"

cli = TCPSocket.new "127.0.0.1", 2827

bobver = cli.gets.chomp[4 .. -1]
puts "BOB version: #{bobver}"
cli.gets

loop do
  puts "\nWhat do you want?"
  puts "1 - ( lookup ) Lookup"
  puts "2 - (  zap   ) Turn off the BOB API"
  puts "3 - (  list  ) List all tunnels that were created using the BOB API"
  puts "4 - ( visit  ) Thread dump to wrapper.log"
  puts "5 - ( verify ) Checks the format of a base64 string"
  puts "e - (        ) Exit"
  print "Option: "
  op = gets.chomp
  puts
  
  case op
  when "1"  # lookup
    puts "Which eepsite should be lookup?"
    eepsite = gets.chomp
    cli.puts "lookup #{eepsite}"
    puts "The base64 from #{eepsite} is '#{cli.gets.chomp[3..-1]}'."
  when "2"  # zap
    cli.puts "zap"
    puts "The BOB API has been deactivated and can be activated again under *127.0.0.1:7657/configclients*."
    puts cli.gets.chomp[3..-1]
    cli.close
    exit
  when "3"  # list
    cli.puts "list"
    fl = cli.gets.chomp
    if fl == "OK Listing done"
      puts "There are currently no tunnels created using the BOB API."
    else
      
      x = 0
      while fl != "OK Listing done"
        mat = /DATA NICKNAME: (.*) STARTING: (.*) RUNNING: (.*) STOPPING: (.*) KEYS: (.*) QUIET: (.*) INPORT: (.*) INHOST: (.*) OUTPORT: (.*) OUTHOST: (.*)/.match(fl)

        puts "Tunnel #{x += 1}"
        puts "    Nickname : #{mat[1]}"
        puts "    Starting : #{mat[2]}"
        puts "    Running  : #{mat[3]}"
        puts "    Stopping : #{mat[4]}"
        puts "    Keys     : #{mat[5]}"
        puts "    Quiet    : #{mat[6]}"
        puts "    Inport   : #{mat[7]}"
        puts "    Inhost   : #{mat[8]}"
        puts "    Outport  : #{mat[9]}"
        puts "    Outhost  : #{mat[10]}"
        fl = cli.gets.chomp
      end
    end
  when "4"  # visit
    cli.puts "visit"
    ans = cli.gets.chomp.chomp " "
    puts "-#{ans}-"
    if ans == "OK"
      puts "Successfully!"
    else
      puts "Execution error: #{ans[6..-1]}"
    end
  when "5"  # verify
    puts "Please enter the base64 string to be checked:"
    cli.puts "verify #{gets.chomp}"
    ans = cli.gets.chomp.chomp " "
    if ans == "OK"
      puts "The base64 string is valid."
    else
      puts "The base64 string is invalid. The following message was returned:"
      puts "    #{ans[6..-1]}"
    end
  when "e"  # exit
    cli.puts "quit"
    puts cli.gets.chomp[3..-1]
    cli.close
    exit
  end
end

